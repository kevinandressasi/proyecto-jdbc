/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package datos;

import java.sql.SQLException;
import java.util.List;
import modelo.UsuarioDTO;

/**
 *
 * @author kevin
 */
public interface UsuarioDAO {
    
    
    public List<UsuarioDTO> seleccionar() throws SQLException;
    
    public int insertar(UsuarioDTO usuarioDTO) throws SQLException;
    
    public int modificar(UsuarioDTO usuarioDTO) throws SQLException;
    
    public int eliminar(UsuarioDTO usuarioDTO) throws SQLException;
    
    
}
