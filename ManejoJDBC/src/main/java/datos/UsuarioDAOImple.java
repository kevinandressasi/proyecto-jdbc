/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package datos;

import static datos.Conexion.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import modelo.UsuarioDTO;

/**
 *
 * @author kevin
 */
public class UsuarioDAOImple implements UsuarioDAO{
    public static final String SQL_SELECT = "SELECT id_user , user_name , pssword FROM curso_java.usuario";
    public static final String SQL_INSERT = "INSERT INTO curso_java.usuario(user_name, pssword) VALUES(?, ?)";
    public static final String SQL_UPDATE = "UPDATE curso_java.usuario SET user_name = ?, pssword = ? WHERE id_user = ?";
    public static final String SQL_DELETE = "DELETE FROM curso_java.usuario WHERE id_user = ?";
    
    
    private Connection conexionTransaccional = null;

    public UsuarioDAOImple() {
    }
    
    public UsuarioDAOImple(Connection conexionTransaccional){
        this.conexionTransaccional = conexionTransaccional;
    }
    
    
    
    public int eliminar(UsuarioDTO usuario) throws SQLException{
        Connection conn = null;
        PreparedStatement ps = null;
        int registros = 0;
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : getConnection();
            ps = conn.prepareStatement(SQL_DELETE);
            
            ps.setInt(1, usuario.getIdUsuario());
            
            registros = ps.executeUpdate();
            
            
        } finally{
            try {
               
                close(ps);
                if(this.conexionTransaccional == null){
                     close(conn);
                }
            } catch (SQLException ex) {
                 ex.printStackTrace(System.out);
            }
            
        }
        
        return registros;
        
        
        
    }
    
    public int modificar(UsuarioDTO usuario) throws SQLException{
     
        Connection conn = null;
        PreparedStatement ps = null;
        int registros = 0;
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : getConnection();
            ps = conn.prepareStatement(SQL_UPDATE);
            
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getPassword());
            ps.setInt(3, usuario.getIdUsuario());
            
            registros = ps.executeUpdate();
            
        } finally{
            try {
                
                close(ps);
                if(this.conexionTransaccional == null){
                    close(conn);
                }
            } catch (SQLException ex) {
               ex.printStackTrace(System.out);
            }
        }
        
        
        return registros;
    }
    
    
    public int insertar(UsuarioDTO usuario) throws SQLException{
        Connection conn = null;
        PreparedStatement ps = null;
        
        int registros = 0;
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : getConnection();
            ps = conn.prepareStatement(SQL_INSERT);
            
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getPassword());
            
            registros = ps.executeUpdate();
        } finally{
                try {
                    
                    close(ps);
                    if(this.conexionTransaccional == null){
                        close(conn);
                    }

                } catch (SQLException ex) {
                     ex.printStackTrace(System.out);
                }
          
        }
        
        return registros;
    }
    
    
    
    public List<UsuarioDTO> seleccionar() throws SQLException{
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        UsuarioDTO usuario = null;
        List<UsuarioDTO> usuarios = new ArrayList<>();
       
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : getConnection();
            ps = conn.prepareStatement(SQL_SELECT);
            rs = ps.executeQuery();
            
           
            while (rs.next()){
                int idUser = rs.getInt("id_user");
                String userName = rs.getString("user_name");
                String pssword = rs.getString("pssword");
                
                usuario = new UsuarioDTO(idUser, userName, pssword);
                usuarios.add(usuario);
            }
        } finally{
            try {
              
                Conexion.close(rs);
                Conexion.close(ps);
                if(this.conexionTransaccional == null){
                    Conexion.close(conn);
                }
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
                
            }
            
        }
        
        
        
        
        return usuarios;
    }
}
