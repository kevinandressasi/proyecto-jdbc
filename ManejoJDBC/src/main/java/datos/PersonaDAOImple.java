/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package datos;

import static datos.Conexion.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import modelo.PersonaDTO;

/**
 *
 * @author kevin
 */
public class PersonaDAOImple implements PersonaDAO {
    
    private Connection conexionTransaccional = null;
    
    
    public PersonaDAOImple(){
        
    }
    public PersonaDAOImple(Connection conexionTransaccional){
        this.conexionTransaccional = conexionTransaccional;
    }
    
    private static final String SQL_SELECT = "SELECT id_persona, nombre, apellido, email, telefono FROM curso_java.persona";
    private static final String SQL_INSERT = "INSERT INTO curso_java.persona(nombre, apellido, email, telefono) VALUES (?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE curso_java.persona SET id_persona= ? , nombre = ? , apellido = ? , email = ? , telefono = ? WHERE id_persona = ?";
    private static final String SQL_DELETE = "DELETE FROM curso_java.persona WHERE id_persona = ?";
    
    
    
    @Override
    public int delete(PersonaDTO persona) throws SQLException{
        Connection conn = null;
        PreparedStatement ps = null;
        
        int registros = 0;
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : getConnection();
            ps = conn.prepareStatement(SQL_DELETE);
            
            ps.setInt(1, persona.getIdPersona());
            
            registros = ps.executeUpdate();
     
        } finally{
            try {
                
                close(ps);
                if(this.conexionTransaccional == null){
                    close(conn);
                }
                
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
            
        }
        
        return registros;
    }
    
    @Override
    public List<PersonaDTO> seleccionar() throws SQLException{
        Connection conn = null;
        PreparedStatement smtm = null;
        ResultSet rs = null;
        PersonaDTO persona = null;
        List<PersonaDTO> personas = new ArrayList<>();
        
        try {
            conn = this.conexionTransaccional != null  ? this.conexionTransaccional: getConnection();
            
            smtm = conn.prepareStatement(SQL_SELECT);
            
            rs = smtm.executeQuery();
            
            while(rs.next()){
                int idPersona = rs.getInt("id_persona");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String email = rs.getString("email");
                String telefono = rs.getString("telefono");
                persona = new PersonaDTO(idPersona,nombre,apellido,email,telefono);
                
                personas.add(persona);
            }
            
            
        } finally{
            try {
                Conexion.close(rs);
                Conexion.close(smtm);
                if(this.conexionTransaccional == null){
                    Conexion.close(conn);
                }
            
            
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        
        return personas;   
    }
    
    
    
    @Override
     public int insertar(PersonaDTO persona) throws SQLException{
            Connection conn = null;
            PreparedStatement smtm = null;
            int registros = 0;
            
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional: Conexion.getConnection();
            smtm = conn.prepareStatement(SQL_INSERT);
            smtm.setString(1, persona.getNombre());
            smtm.setString(2, persona.getApellido());
            smtm.setString(3, persona.getEmail());
            smtm.setString(4, persona.getTelefono());
            
            registros = smtm.executeUpdate();
        } finally{
            
                try {
                  Conexion.close(smtm);
                  if(this.conexionTransaccional == null){
                      Conexion.close(conn);
                  }
                } catch (SQLException ex) {
                    ex.printStackTrace(System.out);
                }
        }
         
         System.out.println("Insercion Realizada Satisfactoriamente.");
            return  registros;
        }
     
     
    @Override
     public int actualizar(PersonaDTO persona) throws SQLException{
         Connection conn = null;
         PreparedStatement smtm = null;
         
         int registros = 0;
         
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : getConnection();
            smtm = conn.prepareStatement(SQL_UPDATE);
           
            smtm.setInt(1, persona.getIdPersona());
            smtm.setString(2, persona.getNombre());
            smtm.setString(3, persona.getApellido());
            smtm.setString(4, persona.getEmail());
            smtm.setString(5, persona.getTelefono());
            smtm.setInt(6, persona.getIdPersona());
            
            registros = smtm.executeUpdate();
                    
                    
                    
        }finally{
             try {
                 
                 close(smtm);
                 if(this.conexionTransaccional == null){
                     close(conn);
                 }
             } catch (SQLException ex) {
                 ex.printStackTrace(System.out);
             }
            
        }
        
         return registros;
     }
}
