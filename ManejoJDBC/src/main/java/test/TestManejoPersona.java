/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import datos.Conexion;
import datos.PersonaDAO;
import datos.PersonaDAOImple;
import datos.UsuarioDAOImple;
import java.sql.*;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.PersonaDTO;
import modelo.UsuarioDTO;

/**
 *
 * @author kevin
 */
public class TestManejoPersona {

    public static void main(String[] args) {
        Connection conexion = null;
        try {
            conexion = Conexion.getConnection();
            if (conexion.getAutoCommit()) {
                conexion.setAutoCommit(false);
            }

           PersonaDAO personaDAO = new PersonaDAOImple(conexion);
           
           
           
           List<PersonaDTO> personasDTO = new ArrayList<>();
           personasDTO = personaDAO.seleccionar();
           
           for(PersonaDTO personaDTO : personasDTO){
               System.out.println("Persona DTO : " + personaDTO);
           }
            
            conexion.commit();
            System.out.println("Se a hecho un commit de la transaccion");
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            System.out.println("Entramos en el rollback");
            try {
                conexion.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace(System.out);
            }
        }

    }
}
