/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestMySqlJDBC {
    public static void main(String[] args) {
        
        
        String url = "jdbc:mysql://localhost:3306/test?useSSL=false&useTimeZone=true&serverTimeZone=UTC&allowPublicKeyRetrieval=true";
        
        try {
//            Class.forName("com.mysql.cj.jdbc.Driver");
            
            Connection conexion = DriverManager.getConnection(url,"root","admin" );
            Statement instrucction = conexion.createStatement(0, 0);
            String sql = "SELECT id_persona, nombre, apellido, email, telefono FROM curso_java.persona";
            ResultSet resultado = instrucction.executeQuery(sql);
            
            while(resultado.next()){
                System.out.println("ID Persona: " + resultado.getInt("id_persona") 
                        + ", Nombre: " + resultado.getString("nombre") 
                        + " , Apellido: " + resultado.getString("apellido") 
                        + " , email: " + resultado.getString("email")
                        + " , telefono: " + resultado.getString("telefono"));
            }
            
            resultado.close();
            instrucction.close();
            conexion.close();
        } catch (SQLException ex) {
           ex.printStackTrace(System.out);
        }
    }
}
