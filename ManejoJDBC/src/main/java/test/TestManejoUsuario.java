/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import datos.Conexion;
import datos.UsuarioDAO;
import datos.UsuarioDAOImple;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.UsuarioDTO;

/**
 *
 * @author kevin
 */
public class TestManejoUsuario {

    public static void main(String[] args) {

        Connection conexion = null;

        try {
            conexion = Conexion.getConnection();

            if (conexion.getAutoCommit() == true) {
                conexion.setAutoCommit(false);
            }
            
          
            UsuarioDAO usuarioDAO =  new UsuarioDAOImple(conexion);
            
            List<UsuarioDTO> usuariosDTO = usuarioDAO.seleccionar();
            
            for(UsuarioDTO usuarioDTO: usuariosDTO){
                System.out.println("Usuarios DTO : " + usuarioDTO);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            System.out.println("Entramos en el Rollback");
            
            try {
                conexion.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace(System.out);
            }
        }

    }
}
